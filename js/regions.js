$(document).ready(function() {
  var statUpdate = 'http://summit.beyondthesummit.tv/referall/php/statUpdate.php';

  var ref = getRefStream();
  $.get("http://ipinfo.io", function(response) {
    var country = response.country, link;
    switch (country) {
      case "US": link = "http://promotions.newegg.com/gigabyte/14-4979/index.html?cm_sp=Cat_Motherboards-_-gigabyte%2f14-4979-_-http%3a%2f%2fpromotions.newegg.com%2fgigabyte%2f14-4979%2f160x360.jpg&icid=278449"; break;
      case "CA": link = "http://www.ncix.com/article/gigabyte_march_intel.htm"; break;
      case "PH": link = "https://www.facebook.com/pages/GIGABYTE-Philippines/119382288144521?ref=hl"; break;
      case "MY": link = "https://www.facebook.com/GIGABYTE.Malaysia?ref=hl"; break;
      case "GB": link = "http://www.overclockers.co.uk/search_results.php?keywords=gigabyte+G1+Gamin&_=1427276127820"; break;
      case "IT": link = "https://www.facebook.com/Gigabyte.Italia?ref=ts&fref=ts"; break;
      case "FR": link = "https://fr-fr.facebook.com/gigabytefr"; break;
      case "IR": link = "http://persian.gigabyte.com/"; break;
      case "EG": link = "https://www.facebook.com/GIGABYTETechnologyEgypt?fref=ts"; break;
      case "SA": link = "http://arabic.gigabyte.com/"; break;
      case "ZA": link = "http://www.gigabyte.com/"; break;
      case "AU": link = "http://www.gigabyte.com.au/MicroSite/366/gaming.html"; break;
      case "TR": link = "https://www.facebook.com/GigabyteTurkiye?ref=br_rs"; break;
      case "RO": link = "https://www.facebook.com/pages/GIGABYTE-ROMANIA/211720038842879"; break;
      case "CZ": link = "https://www.facebook.com/GIGABYTECZSK"; break;
      case "TW": link = "http://g1.gigabyte.com/zh-tw/"; break;
      case "DE": link = "https://www.facebook.com/#!/GBTDeutschland?fref=ts"; break;
      case "BX": link = "https://www.facebook.com/GIGABYTE.Benelux?fref=ts"; break;
      case "BE": link = "https://www.facebook.com/GIGABYTE.Benelux?fref=ts", country = "BX"; break;
      case "NL": link = "https://www.facebook.com/GIGABYTE.Benelux?fref=ts", country = "BX"; break;
      case "LU": link = "https://www.facebook.com/GIGABYTE.Benelux?fref=ts", country = "BX"; break;
      case "SE": link = "http://www.netonnet.se/art/dator/komponenter/moderkort/intel-socket-1150/gigabyte-ga-z97x-gaming3/205235.8086/"; break;
      case "IN": link = "www.facebook.com/GIGABYTEindia"; break;
      case "RU": link = "http://www.gigabyte.ru/"; break;
      case "ES": link = "https://www.facebook.com/GIGABYTESPAIN?ref=ts&fref=ts"; break;
      case "AR": link = "https://www.facebook.com/gigabyte.ar?ref=ts&fref=ts"; break;
      case "PE": link = "https://www.facebook.com/gigabyteperu"; break;
      case "MX": link = "https://www.facebook.com/GIGABYTE.mex"; break;
      case "BR": link = "http://br.gigabyte.com/products/main.aspx?s=42"; break;
      case "PT": link = "https://www.facebook.com/gigabyteportugal?ref=ts&fref=ts"; break;
      case "VN": link = "https://www.facebook.com/GIGABYTE.VN"; break;
      case "KR": link = "www.gigabyte.kr"; break;
      case "PL": link = "https://www.facebook.com/GigabytePoland"; break;
      default: link = "http://g1.gigabyte.com/", country = "UNKOWN";
    }

    $.post(statUpdate+'?ref='+ref+'&c='+country, function(resp){
      console.log(resp);
    });
    
    window.location.replace(link); //redirect user to the proper location
  }, "jsonp");

});

/**
 * getRefStream
 * splits the URL at the query string and returns the refernce stream
 * each stream will have its own referal code, ex BTS is Beyond The Summit
 */
function getRefStream() {
  var ref = window.location.href.split("ref=")[1];
  return ref;
};

